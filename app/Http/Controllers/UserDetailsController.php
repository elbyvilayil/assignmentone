<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\UserDetailsImport;
use App\Http\Requests\ImportCsvFileRequest;

class UserDetailsController extends Controller
{
    /**
     * Return view with data
     *
     * @return void
     */
    public function index()
    {
        $usersDetails = UserDetails::all();
        return view('usersDetails', compact('usersDetails'));
    }

    /**
     * Import User View
     *
     * @return void
     */
    public function importUserDetails()
    {
        return view('user_details.import');
    }

    /**
     * Import User data through sheet
     *
     * @return void
     */
    public function import(ImportCsvFileRequest $request)
    {
        Excel::import(new UserDetailsImport, $request->file('file'));
        return back()->with('success', 'Users imported successfully');
    }
}
