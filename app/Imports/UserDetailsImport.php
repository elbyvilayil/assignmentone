<?php

namespace App\Imports;

use App\Models\UserDetail;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class UserDetailsImport implements ToModel, WithHeadingRow, WithValidation, WithChunkReading, ShouldQueue
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new UserDetail([
           'email'     => $row['email'],
           'person_prefix'    => $row['person_prefix'],
           'firstname' => $row['first_name'],
           'lastname' => $row['last_name'],
           'active' => $row['active']
        ]);
    }
    
    
    public function chunkSize(): int
    {
        return 1;
    }
    
    public function rules(): array
    {
        return [
            'email' => 'required|email:rfc,dns',
            'first_name' => 'required|string',
            'last_name' => 'required|string'
        ];
    }
    
   
}
